var https = require('https');
var http = require('http');
var cheerio = require('cheerio');
var $ = require('jquery-deferred');
var utils = require('./utils');
var _ = require('underscore');

emoticonsList = [];
var fetchEmoticonsList = $.Deferred();

/* Function to retrieve the emoticons list from
 * the HipChat website.
 */
initEmoticonsList = function () {
    https.get('https://www.hipchat.com/emoticons', function (res) {
        var html = '';
        res.on('data', function (chunk) {
            html += chunk;
        });
        res.on('end', function () {
            parseHTML(html);
            fetchEmoticonsList.resolve(emoticonsList);
        });
    }).on('error', function (e) {
        console.log('Got error: ' + e.message);
        fetchEmoticonsList.reject(e);
    });

    function parseHTML(html) {
        c$ = cheerio.load(html);
        var re = /\((\w+)\)/
        c$('div.emoticon-block').each(function (i, elem) {
            emoticonsList.push(re.exec(c$(elem).text())[1]);
        });
    }
};

/* Create HTTP server. The Server has only one valid URI, /api/parse.
 * Everything else returns a 404.
 */
startRestServer = function () {
    var server = http.createServer(function (request, response) {
        var filePath = false;
        if (request.url == '/api/parse') {
            switch (request.method) {
            case 'POST':
                var postData = '';
                request.on('data', function (chunk) {
                    postData += chunk.toString();
                });

                request.on('end', function () {
                    var results = parseRequest(response, postData);
                });
                break;
            default:
                badRequest(response);
            }
        } else {
            notFound(response);
        }
    });
    server.listen(3000);

    function notFound(response) {
        response.writeHead(404, {
            'Content-Type': 'text/plain'
        });
        response.write('Error 404: resource not found.');
        response.end();
    }

    function badRequest(response) {
        response.statusCode = 400;
        response.setHeader('Content-Type', 'text/plain');
        response.end('Bad Request');
    }
}

/* Function to parse the chat message. Parses mentions, emoticons and urls
 * from the message. Additionally, tries to retrieve the title from the URL(s) by 
 * fetching the URL content 
 */
parseRequest = function (response, data) {
    console.log('Parsing data::' + data);
    var mentionsRegex = /@(\w+)/g;
    var emoticonsRegex = /\((\w+)\)/g;
    var urlRegex = /((http(s)?:\/\/)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*))/g;

    var mentions = utils.getAllMatches(data, mentionsRegex);
    
    // Additional filter to exclude emoticons that are not in the list
    // retrieved from the HipChat website
    var emoticons = _.filter(utils.getAllMatches(data, emoticonsRegex), 
                            function(em) {
                                return _.contains(emoticonsList, em);
                            });
    
    var links = utils.getAllMatches(data, urlRegex);

    // Array of promises to track the URL fetch requests
    var deferreds = [];

    _.each(links, function (link) {
        console.log('Retreiving content for link::' + link);
        var deferred = $.Deferred();
        deferreds.push(deferred);

        var getHTML = link.indexOf('https') == -1 ? http.get : https.get

        getHTML(link, function (res) {
            var html = '';
            res.on('data', function (chunk) {
                html += chunk;
            });
            res.on('end', function () {
                parseHTML(html);
            });

            var parseHTML = function (html) {
                c$ = cheerio.load(html);
                var title = c$('title').text();
                title = title.length > 50 ? title.substring(0, 47) + '...' : title;
                deferred.resolve({
                    'url': link,
                    'title': title
                });
            }
        })
        .on('error', function (e) {
            console.log('Got error: ' + e.message);
            deferred.resolve({
                'url': link,
                'title': 'Error retrieving title info.. URL may be invalid'
            });
        });
    });

    /* When all the promises for the URL fetch requests are resolved, invoke the 
     * sendResponse method.
     */
    $.when.apply($, deferreds).done(function () {
        var links = Array.prototype.slice.call(arguments);
        sendResponse(response, {
            'mentions': mentions,
            'emoticons': emoticons,
            'links': links
        });
    });
}

// Sends the response
sendResponse = function (response, results) {
    response.writeHead(200, 'OK', {
        'Content-Type': 'application/json'
    });
    response.end(JSON.stringify(results));
}

//******** Main Program *****************//

// Fetch the emoticons list
initEmoticonsList();

// Start the REST API Server
$.when(fetchEmoticonsList).done(function (data) {
    console.log('Emoticons list successfully fetched..');
}).fail(function () {
    console.log('Error fetching emoticons list..');
}).always(function () {
    startRestServer();
    console.log('REST Service started...');
});