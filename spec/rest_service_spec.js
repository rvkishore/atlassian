var frisby = require('frisby');

// Empty input string
frisby.create('Test the parse API call')
    .post('http://localhost:3000/api/parse', {}, {
        body: ''
    })
    .expectStatus(200)
    .expectHeaderContains('content-type', 'application/json')
    .expectJSON({
            'mentions': [],
            'emoticons': [],
            'links': []
    })
    .toss();

// Test for String with just mentions
frisby.create('Test the parse API call')
    .post('http://localhost:3000/api/parse', {}, {
        body: '@bob @john this is such a cool feature;'
    })
    .expectStatus(200)
    .expectHeaderContains('content-type', 'application/json')
    .expectJSON({
            'mentions': [
                'bob',
                'john'
        ],
            'emoticons': [],
            'links':[]
    })
    .toss();

// Test for String with just emoticons
frisby.create('Test the parse API call')
    .post('http://localhost:3000/api/parse', {}, {
        body: '(success) this is such a cool feature;'
    })
    .expectStatus(200)
    .expectHeaderContains('content-type', 'application/json')
    .expectJSON({
            'mentions': [],
            'emoticons': ['success'],
            'links':[]
    })
    .toss();

// String with invalid emoticons
frisby.create('Test the parse API call')
    .post('http://localhost:3000/api/parse', {}, {
        body: '(success) (chucknorris) (morganfreeman) this is such a cool feature;'
    })
    .expectStatus(200)
    .expectHeaderContains('content-type', 'application/json')
    .expectJSON({
            'mentions': [],
            'emoticons': ['success', 'chucknorris'],
            'links':[]
    })
    .toss();

// Test for String with mentions and emoticons
frisby.create('Test the parse API call')
    .post('http://localhost:3000/api/parse', {}, {
        body: '@bob @john (success) such a cool feature;'
    })
    .expectStatus(200)
    .expectHeaderContains('content-type', 'application/json')
    .expectJSON({
            'mentions': [
                'bob',
                'john'
        ],
            'emoticons': [
                'success'
        ]
    })
    .toss();

// Test for String with mentions, emoticons and urls
frisby.create('Test the parse API call')
    .post('http://localhost:3000/api/parse', {}, {
        body: '@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016 http://www.hipchat.com'
    })
    .expectStatus(200)
    .expectHeaderContains('content-type', 'application/json')
    .expectJSON({
            'mentions': [
                'bob',
                'john'
        ],
            'emoticons': [
                'success'
        ]           
    })
    .expectBodyContains("\"url\":\"https://twitter.com/jdorfman/status/430511497475670016\"")
    .expectBodyContains("\"url\":\"http://www.hipchat.com\"")
    .toss();

// Test for String with invalid urls. The method should return with a title specifying
// that the URL might be invalid
frisby.create('Test the parse API call')
    .post('http://localhost:3000/api/parse', {}, {
        body: '@bob @john (success) such a cool feature; http://www.sdfsdfsdfsd2232324w.com'
    })
    .expectStatus(200)
    .expectHeaderContains('content-type', 'application/json')
    .expectJSON({
            'mentions': [
                'bob',
                'john'
        ],
            'emoticons': [
                'success'
        ]           
    })
    .expectBodyContains("\"url\":\"http://www.sdfsdfsdfsd2232324w.com\"")
    .expectBodyContains("\"title\":\"Error retrieving title info.. URL may be invalid\"")
    .toss();