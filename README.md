# Chat Message Parser

Installation
-------------
1. Clone the git repository contents into a directory.  
   git clone git@bitbucket.org:rvkishore/atlassian.git
2. cd <INSTALL_DIR> 
3. npm install
4. node server.js 

This starts the server on port 3000. The message parsing API is available at /api/parse

To Run the tests
------------------
1. npm install -g jasmine-node
2. jasmine-node spec (Runs the test cases)